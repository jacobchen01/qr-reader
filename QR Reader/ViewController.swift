//
//  ViewController.swift
//  QR Reader
//
//  Created by Jacob Chen on 2/9/20.
//  Copyright © 2020 Jacob Chen. All rights reserved.
//

import UIKit
import AVFoundation //Audio Video Foundation library

class ViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {

    //yellow square in the middle of the screen
    @IBOutlet weak var square: UIImageView!
    var video = AVCaptureVideoPreviewLayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //creating session
        let session = AVCaptureSession()
        
        //define capture device
        guard let captureDevice = AVCaptureDevice.default(for: AVMediaType.video) else {
            print("captureDevice is null")
            return
        }
        
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice)
            session.addInput(input)
        } catch {
            print("ERROR")
        }
        
        let output = AVCaptureMetadataOutput()
        session.addOutput(output)
        
        output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        
        //tells the output that we are only interested in the QR code
        output.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
        
        video = AVCaptureVideoPreviewLayer(session: session)
        video.frame = view.layer.bounds
        view.layer.addSublayer(video)
        
        //shows the square that helps the user aim
        self.view.bringSubviewToFront(square)
        
        session.startRunning()
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        //check if we even have something to process to begin with
        if metadataObjects != nil && metadataObjects.count != 0 {
            //try casting object to a machine readable code object
            if let object = metadataObjects[0] as? AVMetadataMachineReadableCodeObject {
                //check if the object is of type qr code
                if object.type == AVMetadataObject.ObjectType.qr {
                    let alert = UIAlertController(title: "QR Code", message: object.stringValue, preferredStyle: .alert)
                    //add two options to the alert
                    alert.addAction(UIAlertAction(title: "Retake", style: .default, handler: nil))
                    alert.addAction(UIAlertAction(title: "Copy", style: .default, handler: { (nil) in
                        UIPasteboard.general.string = object.stringValue
                    }))
                    
                    //show the alert
                    present(alert, animated: true, completion: nil)
                }
            }
        }
    }


}

